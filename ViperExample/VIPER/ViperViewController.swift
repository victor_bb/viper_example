//
//  ViperViewController.swift
//  ViperExample
//
//  Created by  Виктор Борисович on 25.08.2019.
//  Copyright © 2019 MT. All rights reserved.
//

import UIKit

class ViperViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var categories: [Category] = []
    let categoryCellIdentifier = "CategoryCell"
    var presenter: CategoryPresenterInPut!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = CategoryPresenter()
        // Do any additional setup after loading the view, typically from a nib.
        presenter.output = self
        
        presenter.showCategories()
    }
}

// MARK: - VIPER realization

extension ViperViewController: CategoryPresenterOutPut {
    func updateGreeting(greeting: String) {
        //print("greetingLabel.text = \(greeting)")
    }
    
    func updateCategories(categories: [Category]) {
        self.categories = categories
        self.tableView.reloadData()
    }
}

// MARK: - Table realization

extension ViperViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: categoryCellIdentifier) as! CategoryTableViewCell
        
        let currentCategory = categories[indexPath.row]
        cell.titleLabel.text = currentCategory.name
        cell.countLabel.text = "\(currentCategory.sortOrder)"

        return cell
    }
}
