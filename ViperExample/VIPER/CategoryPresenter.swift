//
//  CategoryPresenter.swift
//  ViperExample
//
//  Created by  Виктор Борисович on 21.08.2019.
//  Copyright © 2019 MT. All rights reserved.
//

import Foundation

protocol CategoryPresenterInPut {
    var output: CategoryPresenterOutPut! { get set }
    
    func nameUpdated(name: String)
    func showCategories()
}

protocol CategoryPresenterOutPut {
    func updateGreeting(greeting: String)
    func updateCategories(categories: [Category])
}

class CategoryPresenter: CategoryPresenterInPut {
    var output: CategoryPresenterOutPut!
    var interactor: CategoryInteractorInPut! = CategoryInteractor()
    var router: CategoryRouterInPut! = CategoryRouter()

    func nameUpdated(name: String) {
        output.updateGreeting(greeting: interactor.greetingForName(name: name))
    }
    
    func showCategories() {
        interactor.getCategories(callBack: output.updateCategories)
    }
}

extension CategoryPresenter: CategoryInteractorOutPut, CategoryRouterOutPut {
    
}
