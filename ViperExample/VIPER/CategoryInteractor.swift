//
//  CategoryInteractor.swift
//  ViperExample
//
//  Created by  Виктор Борисович on 21.08.2019.
//  Copyright © 2019 MT. All rights reserved.
//

import Foundation

protocol CategoryInteractorInPut {
    func greetingForName(name: String) -> String
    func getCategories(callBack: @escaping ([Category]) -> Void)
}

protocol CategoryInteractorOutPut {
    
}

class CategoryInteractor: CategoryInteractorInPut {
    func greetingForName(name: String) -> String {
        return "Hello \(name)"
    }
    
    func getCategories(callBack: @escaping ([Category]) -> Void) {
        StandartLoader().loadCategoriesCallBack(callBack: callBack)
    }
}
