//
//  MvvmViewController.swift
//  ViperExample
//
//  Created by  Виктор Борисович on 25.08.2019.
//  Copyright © 2019 MT. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class MvvmViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let viewModel = CategoryViewModel()
    var categories: [Category] = []
    let categoryCellIdentifier = "CategoryCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.delegate = self
        viewModel.loadCategories()
    }

}

// MARK: - MVVM realization

extension MvvmViewController: CategoryViewModelDelegate {
    func loaded(categories: [Category]) {
        self.categories = categories
        tableView.reloadData()
    }
}

// MARK: - Table realization

extension MvvmViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: categoryCellIdentifier) as! CategoryTableViewCell
        
        let currentCategory = categories[indexPath.row]
        cell.titleLabel.text = currentCategory.name
        cell.countLabel.text = "\(currentCategory.sortOrder)"
        
        return cell
    }
}
