//
//  CategoryViewModel.swift
//  ViperExample
//
//  Created by  Виктор Борисович on 25.08.2019.
//  Copyright © 2019 MT. All rights reserved.
//

import Foundation

protocol CategoryViewModelDelegate {
    func loaded(categories: [Category])
}

class CategoryViewModel {
    
    var delegate: CategoryViewModelDelegate?

    func loadCategories() {
        let loader = StandartLoader()
        loader.delegate = self
        loader.loadCategoriesTask()
    }
}

extension CategoryViewModel: StandartLoaderDelegate {
    func loaded(categories: [Category]) {
        delegate?.loaded(categories: categories)
    }
}
