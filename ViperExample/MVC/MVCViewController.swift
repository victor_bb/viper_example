//
//  MVCViewController.swift
//  ViperExample
//
//  Created by  Виктор Борисович on 25.08.2019.
//  Copyright © 2019 MT. All rights reserved.
//

import UIKit

class MVCViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let viewModel = CategoryViewModel()
    var categories: [Category] = []
    let categoryCellIdentifier = "CategoryCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let loader = StandartLoader()
        loader.delegate = self
        loader.loadCategoriesTask()
    }
}

//

extension MVCViewController: StandartLoaderDelegate {
    func loaded(categories: [Category]) {
        self.categories = categories
        tableView.reloadData()
    }
}

// MARK: - Table realization

extension MVCViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: categoryCellIdentifier) as! CategoryTableViewCell
        
        let currentCategory = categories[indexPath.row]
        cell.titleLabel.text = currentCategory.name
        cell.countLabel.text = "\(currentCategory.sortOrder)"
        
        return cell
    }
}
