//
//  ViewController.swift
//  ViperExample
//
//  Created by  Виктор Борисович on 21.08.2019.
//  Copyright © 2019 MT. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var greetingLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!

    var presenter: CategoryPresenterInPut!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = CategoryPresenter()
        
        // Do any additional setup after loading the view, typically from a nib.
        presenter.output = self
    }

    @IBAction func onTextChanged(_ sender: UITextField) {
        presenter.nameUpdated(name: nameTextField.text ?? "nope")
    }
}

extension ViewController: CategoryPresenterOutPut {
    func updateCategories(categories: [Category]) {
        print("ViewController")
    }
    
    func updateGreeting(greeting: String) {
        greetingLabel.text = greeting
        //print("greetingLabel.text = \(greeting)")
    }
}
